/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package student.sheridan;

/**
 *
 * @author mymac
 */
public class StudentView {
    
    public void printDetails(String name, String iD, String rollNo){
        System.out.println("Student Name: " + name);
        System.out.println("Student ID: " + iD);
        System.out.println("Student Roll Number: " + rollNo);
    }
    
}
