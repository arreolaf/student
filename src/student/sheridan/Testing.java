/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package student.sheridan;

/**
 *
 * @author mymac
 */
public class Testing {
    
    public static void main(String [] args){
        
       Student model = retrieveStudentFromDatabase();
       
       StudentView view = new StudentView();
       
       Controller controller = new Controller(model,view);
       
       controller.updateView();
      
        
    }
    
    public static Student retrieveStudentFromDatabase(){
        
        Student student = new Student();
        student.setName("John");
        student.setRollNo("23");
        student.setID("1234");
        return student;
    }
}
