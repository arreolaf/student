/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package student.sheridan;

/**
 *
 * @author mymac
 */
public class Controller {
    
    private Student model;
    private StudentView view;
    
    Controller (Student model , StudentView view){
        this.model = model;
        this.view = view;
    }
    
    public void setStudentName(String name){
        model.setName(name);
    }
    public String getStudentName(){
        return model.getName();
    }
    
    public void setID(String iD){
        model.setID(iD);
    }
    
    public String getID(){
        return model.getID();
    }
    
    public void setRollNo(String rollNo){
        model.setRollNo(rollNo);
    }
    public String getRollNo(){
        return model.getRollNo();
    }
    
    public  void updateView(){
        
        view.printDetails(model.getName(),model.getID(), model.getRollNo());
    }
    
}
