/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package student.sheridan;

/**
 *
 * @author mymac
 */
public class Student {
    
   private String name; 
   private String iD;
   private String rollNo;
   
   
   public String getName(){
       return name;
   }
   public String getID(){
       return iD;
   }
   public void setName(String name){
       this.name = name;
   }
   public void setID(String iD){
       this.iD= iD;
   }
   public String getRollNo(){
       return rollNo;
   }
   public void setRollNo(String rollNo){
       this.rollNo = rollNo;
   }
   
}
